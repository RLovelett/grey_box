# Toolbox for Grey-box System Identification

#### Dependencies:
- NumPy
- TensorFlow
#### Optional dependendies (to run the demo notebooks):
- Jupyter
- SciPy
- Matplotlib
- Seaborn
- Pandas


![alt text](./figs/GB-ANN_schematic2.svg)

This repository contains codes for grey-box identification of nonlinear dynamical systems using TensorFlow and Keras.
The core components are a set of custom Keras layers. The "Physics" layer will need to be defined independently for any particulary example.
Three example systems are included---a CSTR, a continuous bioreactor, and a continuous bioreactor using prior phenomenalogical information (see companion paper for details).
A second file will need to be generated for each system to define the GB-ANN object---examples are provided for each example system.