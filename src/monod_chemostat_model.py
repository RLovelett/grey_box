'''
This folder contains two classes for the Keras models for ``Grey Box'' Systems of the CSTR
The classes allow for defining, fitting, integratrating, and evaluated the constitutive law
for the model (see demo notebooks)
'''
import numpy as np
import tensorflow as tf
import custom_layers as cl

class monod_chemostat_GB_ANN():
    def __init__(self,D, CGlcIn, mean_z,s_z,mean_phi,s_phi,lag,embed,
                learning_rate = 1e-3):
        '''
        D           : dilution rate
        C_GlcIn     : inlet substrate concentration
        mean_z      : vector of mean values for [E0, S]
        s_z         : vector of standard deviations for [E0, S]
        mean_phi    : estimated mean of the rate constant
        s_phi       : esitmated standard deviation of the rate constant
        lag         : the lag for the input to the ANN model
        embed       : the embedding dimension
        learning_rate : the learning rate for Adam optimizer

        Note: z = [u, x]; ze = embedded z
        '''
        self.output_dim = 4 # four balance equations (X, Glc, IBA, EtOH)
        self.input_dim = 1 # one actuator (light)
        self.n_var = self.output_dim+self.input_dim
        self.embed = embed
        self.lag = lag
        self.mean_z = mean_z
        self.s_z = s_z
        self.mean_phi=mean_phi
        self.s_phi = s_phi

        inputs = tf.keras.Input(shape=(embed*self.n_var,))
        sample_times = tf.keras.Input(shape=(1,))

        # The custom layers
        self.chemostat = cl.monod_chemostat_layer(D,CGlcIn,mean_z[1:5],s_z[1:5],mean_phi,s_phi,mean_z[0], s_z[0])
        self.Prediction = cl.prediction_layer(self.output_dim,self.input_dim)
        self.Interpolation = cl.interpolation_layer(embed,self.output_dim,self.input_dim)
        self.Shift = cl.shift_layer(embed,self.output_dim,self.input_dim)
        self.Select = cl.column_selector_layer(lag,embed,self.n_var)
        self.impute_interp = cl.impute_layer(0.5,self.output_dim,self.input_dim,embed)
        self.impute_shift  = cl.impute_layer(1.0,self.output_dim,self.input_dim,embed)

        # The dense layers for predicting phi
        self.ANN1 = tf.keras.layers.Dense(20,activation=tf.nn.softplus)
        self.ANN2 = tf.keras.layers.Dense(20,activation=tf.nn.softplus)
        self.ANN3 = tf.keras.layers.Dense(20,activation=tf.nn.softplus)

        # The output of the dense layers
        self.ANNout = tf.keras.layers.Dense(3)

        # Get the interpolated inputs and shifted inputs for later use
        interp_inputs = self.Interpolation(inputs)
        shift_inputs  = self.Shift(inputs)

        # Select the lags for first pass
        selected_inputs1 = self.Select(inputs)
        # First pass through dense layers
        phi_k1 = self.ANNout(
                 self.ANN1(
                 self.ANN2(
                 self.ANN3(selected_inputs1))))
        # First pass through the physics model
        k1 = self.chemostat([inputs,phi_k1])

        # use k1 to impute the values for z in interpolated embedding
        interp_inputs2 = self.impute_interp([interp_inputs,inputs,k1])
        # Select the lags for second pass
        selected_inputs2 = self.Select(interp_inputs2)
        # Second pass through the dense layers
        phi_k2 = self.ANNout(
                 self.ANN1(
                 self.ANN2(
                 self.ANN3(selected_inputs2))))
        # Second pass through the physics model
        k2 = self.chemostat([interp_inputs2,phi_k2])

        # use k2 to impute the values for z in interpolated embedding
        interp_inputs3 = self.impute_interp([interp_inputs,inputs,k2])
        # Select the lags for third pass
        selected_inputs3 = self.Select(interp_inputs3)
        # Third pass through the dense layers
        phi_k3 = self.ANNout(
                 self.ANN1(
                 self.ANN2(
                 self.ANN3(selected_inputs3))))
        # Third pass through the physics model
        k3 = self.chemostat([interp_inputs3,phi_k3])

        # use k3 to impute the values for z in shifted embedding
        shift_inputs4 = self.impute_shift([shift_inputs,inputs,k3])
        # Select the lags for fourth pass
        selected_inputs4 = self.Select(shift_inputs4)
        # fourth pass through the dense layers
        phi_k4 = self.ANNout(
                 self.ANN1(
                 self.ANN2(
                 self.ANN3(selected_inputs4))))
        # fourth pass through the physics model
        k4 = self.chemostat([shift_inputs4,phi_k4])

        # RK4 layer for prediction
        outputs = self.Prediction([inputs,k1,k2,k3,k4,sample_times])

        # define model--inherits all Keras model methods (see keras docs for details
        self.chemostat_GB_ANN_model = tf.keras.Model(inputs=[inputs,sample_times],outputs=outputs)

        self.chemostat_GB_ANN_model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),
                                 loss = tf.losses.mean_squared_error)


    def fit(self,x=None, y=None, batch_size=None, epochs=1,verbose=1,callbacks=None,
            validation_data=None, shuffle=True, class_weight=None, sample_weight=None,
            initial_epoch=0, steps_per_epoch=None,validation_steps=None):

        return self.chemostat_GB_ANN_model.fit(x=x, y=y, batch_size=batch_size, epochs=epochs,
                 verbose=verbose,callbacks=callbacks, validation_data=validation_data,
                 shuffle=shuffle, class_weight=class_weight, sample_weight=sample_weight,
                 initial_epoch=initial_epoch, steps_per_epoch=steps_per_epoch,
                 validation_steps=validation_steps)

    def step(self,u_k, x_k, history, step_size=1):
        '''
        Inputs:
        u_k  : The system inputs  (actuators)
        x_k  : The system outputs (conserved quantities)
        history : the ze_k[n_var::]
        step_size : time step size

        Outputs :
        x_k1     : The system outputs at the next time step
        history1 : the system histories at the next time step
        '''
        u_k = tf.reshape(tf.to_float(u_k),[1,1])
        x_k = tf.reshape(tf.to_float(x_k),[1,4])
        history = tf.reshape(tf.to_float(history),[1,self.embed*self.n_var-self.n_var])
        step_size = tf.reshape(tf.to_float(step_size),[1,1])

        ze_k = tf.concat([u_k,x_k,history],axis=1)

        x_k1  = self.chemostat_GB_ANN_model([ze_k,step_size])
        history1 = ze_k[:,0:self.embed*self.n_var-self.n_var]
        return (x_k1,history1)


    def constitutive_law(self,u_k,x_k,history):
        '''
        inputs:
        u_k  : The system inputs  (actuators)
        x_k  : The system outputs (conserved quantities)
        history : the ze_k[n_var::]
        step_size : time step size

        Outputs :
        phi     : The rate law given the system state
        '''
        try:
            N_points = np.shape(u_k)[0]
        except:
            N_points = len(u_k)
        try:
            u_k = tf.reshape(tf.to_float(u_k),[N_points,1])
            x_k = tf.reshape(tf.to_float(x_k),[N_points,4])
            history = tf.reshape(tf.to_float(history),[N_points,self.embed*self.n_var-self.n_var])
        except:
            print('Incorrect input shapes')
        ze_k = tf.concat([u_k,x_k,history],axis=1)
        selected_inputs = self.Select(ze_k)

        phi = self.ANNout(
              self.ANN1(
              self.ANN2(
              self.ANN3(selected_inputs))))

	#Return to physical variables keeping X_k as a shape [none, 1] tensor
        X_k  = ze_k[:,1:5]*self.s_z[1:5]   + self.mean_z[1:5]
        phi  = phi*self.s_phi + self.mean_phi
        U_k  = ze_k[:,0]*self.s_z[0] + self.mean_z[0]


        # Extract variables by name for convenience
        Cx     = tf.reshape(X_k[:,0],(tf.shape(X_k)[0],1))
        CGlc   = tf.reshape(X_k[:,1],(tf.shape(X_k)[0],1))
        CEtOH  = tf.reshape(X_k[:,2],(tf.shape(X_k)[0],1))
        CIBA   = tf.reshape(X_k[:,3],(tf.shape(X_k)[0],1))
        U_k    = tf.reshape(U_k,(tf.shape(X_k)[0],1))
	# Use the Monod equation as an IG for growth
        MU0        = 3e-3*60
        KEtOH      = 7
        KIBA       = 0.7
        Ks         = 1
        MU0IBA_mp  = 0.25
        MU0EtOH_mp = 0.9
        KsIBA      = 1.5
        KsEtOH     = 1.5
        KEtOHEtOH  = 10
        KIBAIBA    = 10

        MU_IG     = MU0*CGlc*tf.exp(-CEtOH/KEtOH - CIBA/KIBA)/(Ks+CGlc)
        MUEtOH_IG = MU0EtOH_mp*CGlc*tf.exp(-CEtOH/KEtOHEtOH)/(KsEtOH+CGlc)
        MUIBA_IG  = MU0IBA_mp*CGlc*tf.exp(-CIBA/KIBAIBA)/(KsIBA+CGlc)

	# Use phis to learn the difference
        MU     = MU_IG * tf.reshape(phi[:,0],(tf.shape(phi)[0],1))
        MUEtOH = MUEtOH_IG * tf.reshape(phi[:,1],(tf.shape(phi)[0],1))
        MUIBA  = MUIBA_IG * tf.reshape(phi[:,2],(tf.shape(phi)[0],1))


        return tf.concat([MU,MUEtOH,MUIBA], axis=1)
