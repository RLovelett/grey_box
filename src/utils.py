# Utility functions for use in Grey-box identification
import numpy as np

def embed_data(ts_data, lag, embed):
    '''
    embeds vectors from an n-dimensional time-series into an n*m dimensional embedding space

    Parameters
    ---------
    ts_data : 2d array [columns=features, rows=time points]
        if 1d, code will reshape to n by 1
    lag : int
        the lag time in sample numbers
    embed : int
        how many lag values to embed: embedding dimension is [embed+1]

    Returns
    _______
    embedding : an n*m dimensional time series in the embedded space



    e.g. for m = 2, lag = l, embed = e, the rows return:
    (x1[k] x2[k] x1[k-l] x2[k-l] x1[k-2l] x2[k-2l] ... x1[k-l(e-1)] x2[k-l(e-1)])

    '''
    # Manipulate variable order for desired output form
    ts_data = np.flip(ts_data, axis=1)

    # Check dimensionality of input; should be 2
    input_dims = len(np.shape(ts_data))
    if input_dims == 1:
        n_pts = len(ts_data)
        ts_data.shape = (n_pts,1)
    if input_dims > 2:
        raise Exception('Must input 1D or 2D Numpy array')

    # number of input time points; number of input dimensions
    (tsize,n) = ts_data.shape

    # number of output time_points)
    t_iter = tsize-(lag*(embed-1))

    # number of output dimension
    n_feat = embed*n

    embedding = np.zeros((t_iter,n_feat))

    for i in range(t_iter):
        end_val = i+lag*(embed-1)+1

        part = ts_data[i:end_val]
        embedding[i,:] = part[::lag].flatten()

    return np.flip(embedding,axis=1)
