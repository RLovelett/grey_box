'''
Grey-box identification modules uses the Keras functional API
in combinition with a set of built-in and  custom-defined layers.

Every grey-box model will require a column selector layer, an interpolation layer,
a shift layer, two impute layers, a prediction layer (Runge-Kutta equations),
a series of Dense layers, and a physics layer. The physics layer will have to be custom
written for each particular application. Two example physics layers (CSTR and
chemostat) are included here.

Note: if necessary, any transformation from physical to normalized/standardized
variables should exist within the physics layer. The input/output
behavior of the model should be in normalized/standardized variables.

The actual models are defined in the Keras functional API; example models
are shown in the models.py file
'''

import numpy as np
import tensorflow as tf

class column_selector_layer(tf.keras.layers.Layer):
    def __init__(self,lag,embed,n_var):
        '''
        layer takes ze_k (which uses lag=1) as a input and
        outputs the correct columns for the specified lag time
        '''
        super(column_selector_layer,self).__init__()
        self.lag = lag
        self.n_var = n_var

        # build the column selector at build time, not run time
        if embed==1:
            indices = np.arange(0,n_var)
        else:
            indices = []
            for i in range(n_var):
                indices = indices+[np.arange(0,embed*n_var, lag*n_var)+i]
        self.columns = np.sort(np.hstack(indices))



    def build(self,input_shape):
        super(column_selector_layer,self).build(input_shape)

    def compute_output_shape(self,input_shape):
        return tf.TensorShape([input_shape[0],len(self.columns)])

    def call(self,inputs):
        '''
        inputs : ze_k
        output : z_selected_k
        '''
        ze_k = inputs


        # select desired columns
        layer_output = tf.transpose(tf.gather(tf.transpose(ze_k),self.columns))

        return layer_output

class interpolation_layer(tf.keras.layers.Layer):
    '''
    Layer takes ze_k as an input and computes
    ze_[k+.5k], the interpolated ze_k at using pairwise averaging.
    it does not include the first n_var elements (which will be
    appended later in an impute layer based on RK approx)
    '''
    def __init__(self, embed,
                       output_dimensionality,
                       input_dimensionality):
        '''
        embed : the embedding dimensionality
        output_dimensionality : the number of conserved physical quantities (PHYS SYS OUTPUTS)
        input_dimensionality  : the number of actuators (PHYS SYS INPUTS)
        '''
        super(interpolation_layer,self).__init__()
        self.embed = embed
        self.output_dim = output_dimensionality
        self.input_dim = input_dimensionality
        self.n_var = output_dimensionality+input_dimensionality
        if embed>1:
            self.layer_output_dim = embed*self.n_var-self.n_var
        else:
            self.layer_output_dim = self.n_var

    def build(self,input_shape):
        super(interpolation_layer,self).build(input_shape)

    def compute_output_shape(self,input_shape):
        return tf.TensorShape([input_shape[0],self.layer_output_dim])

    def call(self,inputs):
        '''
        inputs : [ze_k] (K is RK4 coeff 1 or 2 to estimate midpoint)
        output : ze_k shifted by  0.5 (the first n_var elements will be added later in an IMPUTE layer
        '''

        # Extract tensors
        ze_k = inputs

        if self.embed==1:
            return ze_k

        # linear interpolation to find the midpoint of the embeddings
        for i in range(self.embed-1):
            if i == 0:
                layer_output = 0.5*(ze_k[:,self.n_var*i:self.n_var*i+self.n_var]+ze_k[:,self.n_var*i+self.n_var:self.n_var*i+2*self.n_var])
            else:
                layer_output = tf.concat([layer_output,
                               0.5*(ze_k[:,self.n_var*i:self.n_var*i+self.n_var]+ze_k[:,self.n_var*i+self.n_var:self.n_var*i+2*self.n_var])],
                               axis = 1)
        return layer_output

class shift_layer(tf.keras.layers.Layer):
    '''
    Layer takes ze_k and compute ze[k+1], with the first n_var elements
    to be imputed later (based on RK approx)
    '''

    def __init__(self, embed,
                       output_dimensionality,
                       input_dimensionality):
        '''
        embed : the embedding dimensionality
        output_dimensionality : the number of conserved physical quantities (PHYS SYS OUTPUTS)
        input_dimensiionality : the number of actuators (PHYS SYS INPUTS)
        '''
        super(shift_layer,self).__init__()
        self.embed = embed
        self.output_dim = output_dimensionality
        self.input_dim = input_dimensionality
        self.n_var = output_dimensionality+input_dimensionality
        if embed>1:
            self.layer_output_dim = embed*self.n_var-self.n_var
        else:
            self.layer_output_dim = self.n_var

    def build(self,input_shape):
        super(shift_layer,self).build(input_shape)

    def compute_output_shape(self,input_shape):
        return tf.TensorShape([input_shape[0],self.layer_output_dim])

    def call(self,inputs):
        '''
        inputs : [ze_k]
        output : ze_k shifted by 1 time step--first n_var elements need to be imputed in impute layer
        '''

        # extract tensors
        ze_k = inputs

        if self.embed==1:
            return ze_k

        # time shift
        layer_output = ze_k[:,0:self.embed*self.n_var-self.n_var]

        return layer_output

class prediction_layer(tf.keras.layers.Layer):
    '''
    Layer computes x_[k+1] given inputs ze_k (embedded [u_k x_k]) and k1, k2, k3, k4 (the RK4 coefficients)
    '''
    def __init__(self,output_dimensionality, input_dimensionality):
        '''
        output_dimensionality : the number of conserved quantities in the physics equations (treated as "outputs")
        input_dimensionality : the number of actuators
        '''
        super(prediction_layer,self).__init__()
        self.output_dim= output_dimensionality
        self.input_dim = input_dimensionality

    def build(self,input_shape):
        super(prediction_layer,self).build(input_shape)

    def compute_output_shape(self,input_shape):
        return tf.TensorShape([input_shape[0][0],self.output_dim])

    def call(self,inputs):
        '''
        inputs : the concatenation of ze_k and the RK4 coefficients
        output : xk+1 using RK4 approximation
        '''

        # Extract the individual tensors
        zk = inputs[0]
        k1 = inputs[1]
        k2 = inputs[2]
        k3 = inputs[3]
        k4 = inputs[4]
        sample_time = inputs[5]

        xk = zk[:,self.input_dim:self.input_dim+self.output_dim]

        # xk+1 using the RK4 approximation
        xk1 = xk + sample_time*(k1+2*k2+2*k3+k4)/6
        return xk1

class impute_layer(tf.keras.layers.Layer):
    '''
    Layer imputes the predicted z into interpolated or shifted ze using the RK estimates)
    Each model will have 2 impute layers, one for interpolation, one for shifting,
    defined at build time
    '''
    def __init__(self,time_step,output_dimensionality, input_dimensionality,embed):
        '''
        output_dimensionality : the number of conserved quantities in the physics equations (treated as "outputs")
        input_dimensionality : the number of actuators
        time_step : 0.5 for interp layer, 1 for shift layer
        '''
        super(impute_layer,self).__init__()
        self.output_dim= output_dimensionality
        self.input_dim = input_dimensionality
        self.n_var = input_dimensionality+output_dimensionality
        if time_step != 1.0 and time_step != 0.5:
            import warnings
            warnings.warn('Time step should be 0.5 for interpolation or 1.0 for shift')
        self.time_step = time_step
        self.embed = embed

    def build(self,input_shape):
        super(impute_layer,self).build(input_shape)

    def compute_output_shape(self,input_shape):
        if self.embed>1:
            return input_shape[0]
        else:
            return input_shape[1]


    def call(self,inputs):
        '''
        inputs : [interpolated or shifted ze_k with upfront zeros; orig input; K]
        output : interpolated or shifted ze_k with RK approximations
        '''

        # Extract the individual tensors
        ze_ks = inputs[0]
        ze_k  = inputs[1]
        k = inputs[2]

        # define the system inputs tensor (assume stays constant)
        sys_in_next = ze_k[:,0:self.input_dim]
        # define the system outputs (assuming RK approx)
        sys_out_next = ze_k[:,self.input_dim:self.n_var] + self.time_step*k

        # impute the new data
        if self.embed>1:
            layer_output = tf.concat([sys_in_next,sys_out_next,ze_ks],axis=1)
        else:
            layer_output = tf.concat([sys_in_next,sys_out_next],axis=1)
        return layer_output

class CSTR_layer(tf.keras.layers.Layer):
    '''
    A physics layer containing the balance equations for the CSTR containing a
    single S-->P reaction catalyzed by E (rate law from ANN)
    '''

    def __init__(self,theta,S0, mean_x, s_x, mean_phi, s_phi):
        '''
        specifiy the known constants on initializatoion
        theta    : the average residence time
        S0       : the inlet subtrate concentration
        mean_x   : the vector of mean values for [S]
        s_x      : the vector of standard deviations for [S]
        mean_phi : the (estimated) mean of the rate constant
        s_phi    : the (estimated) standard deviation of the rate constant

        use the same means, s's as used to standardized data on input---here they are used to unstandardize back to physical variables
        '''
        super(CSTR_layer,self).__init__()
        self.theta       = theta
        self.S0          = S0
        self.mean_x      = mean_x
        self.s_x         = s_x
        self.mean_phi    = mean_phi
        self.s_phi       = s_phi

    def build(self, input_shape):
        super(CSTR_layer,self).build(input_shape)

    def call(self,inputs):
        '''
        inputs : [z_k, phi_model]  where z_k is [inputs, outputs] and [phi_model] is prediction from dense ANN layers
        '''

        # Extract tensors:
        ze_k = inputs[0]
        phi_s = inputs[1]

        #Return to physical variables keeping S as a shape [none, 1] tensor
        S   = tf.reshape(ze_k[:,1]*self.s_x   + self.mean_x, tf.shape(phi_s))
        phi = phi_s*self.s_phi + self.mean_phi

        # Balance equation:
        dS  = ((self.S0-S)/self.theta - phi)

        # return to standardized variables:
        dS_std = dS/self.s_x

        return dS_std



class chemostat_layer(tf.keras.layers.Layer):
    '''
    A physics layer containing the balance equations for the CSTR containing a
    single S-->P reaction catalyzed by E (rate law from ANN)
    '''

    def __init__(self,D, CGlcIn, mean_x, s_x, mean_phi, s_phi):
        '''
        specifiy the known constants on initializatoion
        D             : the dilution time
        CGlc_In       : the inlet glucose concentration
        mean_x   : the vector of mean values for [S]
        s_x      : the vector of standard deviations for [S]
        mean_phi : the (estimated) mean of the rate constant
        s_phi    : the (estimated) standard deviation of the rate constant

        use the same means, s's as used to standardized data on input---here they are used to unstandardize back to physical variables
        '''
        super(chemostat_layer,self).__init__()
        self.D        = D
        self.CGlcIn   = CGlcIn
        self.mean_x   = mean_x
        self.s_x      = s_x
        self.mean_phi = mean_phi
        self.s_phi    = s_phi

    def build(self, input_shape):
        '''
        Add weights here for the learned yield coefficients
        '''
        self.YGlcX     = self.add_weight("YGlcX",
                                         shape=[1],
                                         trainable=True,
                                         constraint=tf.keras.constraints.NonNeg(),
                                         initializer = tf.keras.initializers.RandomUniform(minval=0.0,
                                                                                           maxval=1.0,))
        self.YGlcEtOH  = self.add_weight("YGlcX",
                                         shape=[1],
                                         trainable=True,
                                         constraint=tf.keras.constraints.NonNeg(),
                                         initializer = tf.keras.initializers.RandomUniform(minval=0.0,
                                                                                           maxval=1.0,))
        self.YGlcIBA   = self.add_weight("YGlcX",
                                         shape=[1],
                                         trainable=True,
                                         constraint=tf.keras.constraints.NonNeg(),
                                         initializer = tf.keras.initializers.RandomUniform(minval=0.0,
                                                                                           maxval=1.0,))

        super(chemostat_layer,self).build(input_shape)


    def call(self,inputs):
        '''
        inputs : [z_k, phi_model]  where z_k is [inputs, outputs] and [phi_model] is prediction from dense ANN layers
        '''

        # Extract tensors:
        ze_k = inputs[0]
        phi_s = inputs[1]

        #Return to physical variables keeping X_k as a shape [none, 1] tensor
        X_k  = ze_k[:,1:5]*self.s_x   + self.mean_x
        phi  = phi_s*self.s_phi + self.mean_phi

        # Extract variables by name for convenience
        Cx     = tf.reshape(X_k[:,0],(tf.shape(X_k)[0],1))
        CGlc   = tf.reshape(X_k[:,1],(tf.shape(X_k)[0],1))
        CEtOH  = tf.reshape(X_k[:,2],(tf.shape(X_k)[0],1))
        CIBA   = tf.reshape(X_k[:,3],(tf.shape(X_k)[0],1))
        MU     = tf.reshape(phi[:,0],(tf.shape(phi)[0],1))
        MUEtOH = tf.reshape(phi[:,1],(tf.shape(phi)[0],1))
        MUIBA  = tf.reshape(phi[:,2],(tf.shape(phi)[0],1))

        # Balance equations:

        Glucose_Consumption = -Cx*(MU/self.YGlcX + MUEtOH/self.YGlcEtOH + MUIBA/self.YGlcIBA)
        dX = tf.concat([(MU-self.D)*Cx,
                        Glucose_Consumption + (self.CGlcIn - CGlc)*self.D,
                        MUEtOH*Cx - self.D*CEtOH,
                        MUIBA*Cx  - self.D*CIBA],axis=1)


        # return to standardized variables:
        dX_std = dX/self.s_x

        return dX_std


class monod_chemostat_layer(tf.keras.layers.Layer):
    '''
    A physics layer containing the balance equations for the chemostat
    that used the Monod equation as an I.G. for the rate laws
    '''

    def __init__(self,D, CGlcIn, mean_x, s_x, mean_phi, s_phi,mean_u,s_u):
        '''
        specifiy the known constants on initializatoion
        D             : the dilution time
        CGlc_In       : the inlet glucose concentration
        mean_x   : the vector of mean values for [S]
        s_x      : the vector of standard deviations for [S]
        mean_phi : the (estimated) mean of the rate constant
        s_phi    : the (estimated) standard deviation of the rate constant

        use the same means, s's as used to standardized data on input---here they are used to unstandardize back to physical variables
        '''
        super(monod_chemostat_layer,self).__init__()
        self.D        = D
        self.CGlcIn   = CGlcIn
        self.mean_x   = mean_x
        self.s_x      = s_x
        self.mean_phi = mean_phi
        self.s_phi    = s_phi
        self.mean_u   = mean_u
        self.s_u      = s_u

    def build(self, input_shape):
        '''
        Add weights here for the learned yield coefficients
        '''
        self.YGlcX     = self.add_weight("YGlcX",
                                         shape=[1],
                                         trainable=True,
                                         constraint=tf.keras.constraints.NonNeg(),
                                         initializer = tf.keras.initializers.RandomUniform(minval=0.0,
                                                                                           maxval=1.0,))
        self.YGlcEtOH  = self.add_weight("YGlcX",
                                         shape=[1],
                                         trainable=True,
                                         constraint=tf.keras.constraints.NonNeg(),
                                         initializer = tf.keras.initializers.RandomUniform(minval=0.0,
                                                                                           maxval=1.0,))
        self.YGlcIBA   = self.add_weight("YGlcX",
                                         shape=[1],
                                         trainable=True,
                                         constraint=tf.keras.constraints.NonNeg(),
                                         initializer = tf.keras.initializers.RandomUniform(minval=0.0,
                                                                                           maxval=1.0,))

        super(monod_chemostat_layer,self).build(input_shape)


    def call(self,inputs):
        '''
        inputs : [z_k, phi_model]  where z_k is [inputs, outputs] and [phi_model] is prediction from dense ANN layers
        '''

        # Extract tensors:
        ze_k = inputs[0]
        phi_s = inputs[1]

        #Return to physical variables keeping X_k as a shape [none, 1] tensor
        X_k  = ze_k[:,1:5]*self.s_x   + self.mean_x
        phi  = phi_s*self.s_phi + self.mean_phi
        U_k  = ze_k[:,0]*self.s_u + self.mean_u


        # Extract variables by name for convenience
        Cx     = tf.reshape(X_k[:,0],(tf.shape(X_k)[0],1))
        CGlc   = tf.reshape(X_k[:,1],(tf.shape(X_k)[0],1))
        CEtOH  = tf.reshape(X_k[:,2],(tf.shape(X_k)[0],1))
        CIBA   = tf.reshape(X_k[:,3],(tf.shape(X_k)[0],1))
        U_k    = tf.reshape(U_k,(tf.shape(X_k)[0],1))

        # Use the Monod equation as an IG for growth
        MU0        = 3e-3*60
        KEtOH      = 7
        KIBA       = 0.7
        Ks         = 1
        MU0IBA_mp  = 0.25
        MU0EtOH_mp = 0.9
        KsIBA      = 1.5
        KsEtOH     = 1.5
        KEtOHEtOH  = 10
        KIBAIBA    = 10

        MU_IG     = MU0*CGlc*tf.exp(-CEtOH/KEtOH - CIBA/KIBA)/(Ks+CGlc)
        MUEtOH_IG = MU0EtOH_mp*CGlc*tf.exp(-CEtOH/KEtOHEtOH)/(KsEtOH+CGlc)
        MUIBA_IG  = MU0IBA_mp*CGlc*tf.exp(-CIBA/KIBAIBA)/(KsIBA+CGlc)

        # Use phis to learn the difference
        MU     = MU_IG*tf.reshape(phi[:,0],(tf.shape(phi)[0],1))
        MUEtOH = MUEtOH_IG*tf.reshape(phi[:,1],(tf.shape(phi)[0],1))
        MUIBA  = MUIBA_IG*tf.reshape(phi[:,2],(tf.shape(phi)[0],1))


        # Balance equations:

        Glucose_Consumption = -Cx*(MU/self.YGlcX + MUEtOH/self.YGlcEtOH + MUIBA/self.YGlcIBA)
        dX = tf.concat([(MU-self.D)*Cx,
                        Glucose_Consumption + (self.CGlcIn - CGlc)*self.D,
                        MUEtOH*Cx - self.D*CEtOH,
                        MUIBA*Cx  - self.D*CIBA],axis=1)


        # return to standardized variables:
        dX_std = dX/self.s_x

        return dX_std
